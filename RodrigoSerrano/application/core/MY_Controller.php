<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

   	public function show($html, $dados = NULL, $menu = true){
        $result = $this->load->view('common/header', $dados, true);
        $menu ? $result .= $this->load->view('common/navbar', $dados, true) : null;
        
        $result .= $html;

        $result .= $this->load->view('common/footer', null, true);   
        
        echo $result;
    }

}
