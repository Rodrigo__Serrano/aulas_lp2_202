<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}

	// public function mdb(){
	// 	$this->load->view('common/cabecalho');
	// 	$this->load->view('introducao/mdb_intro');
	// 	$this->load->view('common/rodape');
	// }

    public function login(){
        $this->load->model('LoginModel', 'login');
		$dados['error'] = $this->login->LogUserIn();
		$dados['title'] = 'Login';
		
		$html = $this->load->view('access/login_form', $dados, true);
		
		$this->show($html, $dados, false);

        //$html .= $this->load->view('access/footer', null, true);
	}
	
	// private function show($html){
	// 	$result = $this->load->view('common/header', null, true);
	// 	//$result .= $this->load->view('common/navbar', null, true);
		
    //     $result .= $html;

    //     $result .= $this->load->view('common/footer', null, true);   
        
    //     echo $result;
    // }
}
