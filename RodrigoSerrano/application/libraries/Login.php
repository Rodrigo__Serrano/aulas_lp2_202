<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries/util/CI_Object.php';

class Login extends CI_Object {
    
    public function verifica_user($email, $senha){
        $sql = $this->db->get_where(
            'login', [
            'email' => $email, 
            'senha' => $senha
        ]);
        
        $rs = $sql->result_array();
        return $rs;
    }
}
