<div class="container mb-5"></div>
    <div class="row">
        <div class="col-md-6 mx-auto border pb-3">
            <form class="mt-5" method="POST" id="contas-form">

                <input type="text" id="form-control" name="parceiro" class="form-control" placeholder="Devedor / Credor"><br>
                <input type="text" id="form-control" name="descricao" class="form-control" placeholder="Descrição"><br>
                <input type="number" id="form-control" name="valor" class="form-control" placeholder="Valor"><br><br>
                <input type="hidden" name="tipo" value="<?= $tipo ?>">


                <div class="text-center text-md-left">
                    <a class="btn btn-dark btn-block" onclick="document.getElementById('contas-form').submit();">Enviar</a>
                </div>
            </form>
        </div>
    </div>
</div>