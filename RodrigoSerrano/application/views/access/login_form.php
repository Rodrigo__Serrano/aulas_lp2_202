<div style="height: 100vh">
    <div class="flex-center flex-column">
        <h3 class="mb-5">Controle Financeiro Pessoal</h3>
        <?php if($error): ?>
        <h3><span class="badge badge-danger">
            Dados de acesso incorretos.
        </h3>
        <?php endif; ?>
            <form class="text-center border border-light p-5" method="POST">
                <p class="h4 mb-4">Entrar</p>
                <input type="email" name="email" class="form-control mb-4" placeholder="Email">
                <input type="password" name="senha" class="form-control mb-4" placeholder="Senha">
                <button class="btn btn-dark btn-block my-4" type="submit">Login</button>
            </form>
        </div>
    </div>
